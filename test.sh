for distro in alpine:latest python:3.9-slim
do
    echo "testing: $distro"
    docker run -it --rm -v $PWD:/app -w /app $distro sh addca.sh
done

#!/bin/sh
# addca.sh
# script to augment system and utility CA root store with a given CA cert or URL to pull CA from
# supported:
#   - OS roots
#   - python / certifi



if [ -z "$1" ]; then
    echo "Pass path to custom CA as an argument: ./addca.sh /foo/cacert.crt"
    exit 
fi

CUSTOM_CA=$1


# Set OS system trust details
# shamelessly cribbed from mkcert ref: https://github.com/FiloSottile/mkcert/blob/master/truststore_linux.go
SYSTEM_TRUST_FILENAME=""
SYSTEM_TRUST_COMMAND=""

SYSTEM_CONCAT="false"
if [ -d /etc/pki/ca-trust/source/anchors/ ]; then
SYSTEM_TRUST_FILENAME="/etc/pki/ca-trust/source/anchors/99-addca.pem"
SYSTEM_TRUST_COMMAND="update-ca-trust extract"
elif [ -d /usr/local/share/ca-certificates ]; then
SYSTEM_TRUST_FILENAME="/usr/local/share/ca-certificates/99-addca.crt"
SYSTEM_TRUST_COMMAND="update-ca-certificates"
elif [ -d /etc/ca-certificates/trust-source/anchors/ ]; then
SYSTEM_TRUST_FILENAME="/etc/ca-certificates/trust-source/anchors/99-addca.crt"
SYSTEM_TRUST_COMMAND="trust extract-compat"
elif [ -d /usr/share/pki/trust/anchors ]; then
SYSTEM_TRUST_FILENAME="/usr/share/pki/trust/anchors/99-addca.pem"
SYSTEM_TRUST_COMMAND="update-ca-certificates"
elif [ -f /etc/ssl/certs/ca-certificates.crt ]; then
# must add our CA to an existing file
SYSTEM_CONCAT="true"
SYSTEM_TRUST_FILENAME="/etc/ssl/certs/ca-certificates.crt"
SYSTEM_TRUST_COMMAND="cat $CUSTOM_CA >> $SYSTEM_TRUST_FILENAME"
fi

if [ -z "$SYSTEM_TRUST_FILENAME" ]; then
    echo "Not sure where the trust store is on this system, sorry!"
    exit 1
fi


echo $SYSTEM_TRUST_FILENAME
echo $SYSTEM_TRUST_COMMAND

if [ $(id -u) -gt 0 ];then
    echo "Must run as root! Am $(id)"
    exit 1
fi

if [ "$SYSTEM_CONCAT" = "false" ]; then
    cp "$CUSTOM_CA" "$SYSTEM_TRUST_FILENAME"
fi

echo "running: $SYSTEM_TRUST_COMMAND"

$SYSTEM_TRUST_COMMAND

## Set up non-system bundle for other utilities like python, git, etc
# cURL maintains a mirror of Firefox's built-in ca certs. We'll use it as our base trust store.
# we'll use it for any utilities that don't use the OS cert store
BASE_CA="https://curl.se/ca/cacert.pem"
BASE_CA_BUNDLE=/addca/cacert.pem
CUSTOM_BUNDLE=/addca/addca-bundle.crt

mkdir -p $(dirname "$BASE_CA_BUNDLE")
cp "$CUSTOM_CA" "$(dirname "$BASE_CA_BUNDLE")/addca.crt"

curl -Ls "$BASE_CA" > "$BASE_CA_BUNDLE"
cat "$BASE_CA_BUNDLE" "$CUSTOM_CA" > "$CUSTOM_BUNDLE"


## Config various utilities to use that CUSTOM_BUNDLE
if which pip ; then
   pip config --global set global.cert "$CUSTOM_BUNDLE"
fi

if which git ; then
    git config --global http.sslVerify "true"
    git config --global http.sslCAInfo "$CUSTOM_BUNDLE"
fi

echo "For python, set REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt"
echo "For NodeJS set NODE_EXTRA_CA_CERTS=/addca/addca.crt"

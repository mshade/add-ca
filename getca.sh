#!/bin/bash

if [ -z $1 ]; then
  echo "Supply hostname to get cert from as an argument: ./getca.sh foo.exampl.com "
  exit 1
fi

echo quit | openssl s_client -showcerts -servername "$1" -connect "$1":443 > cacert.pem

echo -e "\nTesting the cert we just got with curl...\n"

curl --cacert cacert.pem https://$1/
